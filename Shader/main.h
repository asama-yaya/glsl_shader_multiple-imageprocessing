#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <math.h>
#include <time.h>

using std::cout;
using std::endl;

using std::vector;
using std::string;
using std::to_string;
using std::stoi;
using std::stod;
using std::stoul;

using std::ifstream;
using std::ofstream;
using std::ios;

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定
// OpenGL --------------------------------------------------------------------------------------
#include <glew.h>
#include <glut.h>
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "glut32.lib")
// MyHeader ------------------------------------------------------------------------------------
#pragma once
#include "AsaVertex.h"
#include "AsaTrackball.h"
#include "OBJLoader.h"

//**********************************************************************************************
// variable
//**********************************************************************************************
int window_width = 640;
int window_height = 480;

TTrackball Trackball;
OBJMESH Objmesh;

// flag
bool capture_flag = false;
int render_number = 10;

// shader
GLuint glProgram;

GLuint multiProgram;
static GLuint fb_multi;           // フレームバッファオブジェクト
static GLuint cb_multi;           // カラーバッファ用のテクスチャ
static GLuint rb_multi;           // デプスバッファ用のレンダーバッファ
static GLuint sb_multi;           // 鏡面反射光
static GLuint ab_multi;           // 環境光の反射光
static const GLenum bufs_multi[] = {
	GL_COLOR_ATTACHMENT0_EXT, //   カラーバッファ (拡散反射光)
	GL_COLOR_ATTACHMENT1_EXT, //   鏡面反射光
	GL_COLOR_ATTACHMENT2_EXT, //   環境光の反射光
};
GLuint multiTexId = -1;

GLuint ofsProgram;
static GLuint fb_ofs;           // フレームバッファオブジェクト
static GLuint cb_ofs;           // カラーバッファ用のテクスチャ
static GLuint rb_ofs;           // デプスバッファ用のレンダーバッファ
static const GLenum bufs_ofs[] = {
	GL_COLOR_ATTACHMENT0_EXT, //   カラーバッファ (拡散反射光)
};
GLuint ofsTextId = -1;

//**********************************************************************************************
// Method
//**********************************************************************************************
// shader
void InitShader();

// shader 初期化関連
void LoadGLTexture(string _fileName, GLuint &_texID, const bool _rectFlag = false);
void SetTexture(GLuint &_texID, const int _w, const int _h, const bool _rectFlag = false);
void MakeShader(GLuint &_gl2Program, string _vertName, string _fragName);
int ReadShaderSource(GLuint _shader, const char *_file);
void PrintShaderInfoLog(GLuint _shader);
void PrintProgramInfoLog(GLuint _program);

// glの関数
void display(void);
void resize(int _w, int _h);
void mouse(int _button, int _state, int _x, int _y);
void motion(int _x, int _y);
void keyboard(unsigned char _key, int _x, int _y);
void idle(void);