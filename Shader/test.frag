#version 330
// shadertype=<glsl>
// frag

// テクスチャ
uniform sampler2DRect image;

// フレームバッファに出力するデータ
layout (location = 0) out vec4 fc1; 

void main(void){
	fc1 = texture(image, gl_FragCoord.xy);
}

