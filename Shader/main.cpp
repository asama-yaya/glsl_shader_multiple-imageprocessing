#include "main.h"


int main(int argc, char *argv[]){

	// glutのコールバック関数
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("MainWindow");
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);

	// 初期化
	glClearColor(0.0f, 0.3f, 0.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);

	glDisable(GL_CULL_FACE);					// 両面
	//glEnable(GL_CULL_FACE);					// 表のみ
	//glCullFace(GL_BACK);
	//glEnable(GL_CULL_FACE);					// 裏のみ
	//glCullFace(GL_FRONT);

	// shader init
	InitShader();

	// obj load
	if (!Objmesh.LoadFile("data\\texture.obj")){
		getchar();
		exit(0);
	}

	// main loop
	glutMainLoop();
	return 0;
}

// display
void display(void){

	// GPUに送る行列用
	GLfloat gl_pc[16], gl_m[16], gl_normal[9];


	//**********************************************************
	// multiple render
	//**********************************************************
	// 描画先をフレームバッファオブジェクトに切り替える
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb_multi);

	// レンダーターゲットを指定する
	glDrawBuffers(sizeof bufs_multi / sizeof bufs_multi[0], bufs_multi);

	// 画面クリア
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// viewport
	glViewport(0, 0, window_width, window_height);

	// シェーダ選択
	glUseProgram(multiProgram);

	// textureの有効化
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, multiTexId);

	// 透視投影うんぬん
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (double)window_width / window_height, 0.1, 1200.0);
	gluLookAt(0, 0, 200, 0, 0, 0, 0, 1, 0);

	// projection matrixをぽーい
	glGetFloatv(GL_PROJECTION_MATRIX, gl_pc);
	glUniformMatrix4fv(glGetUniformLocation(multiProgram, "matPC"), 1, GL_FALSE, gl_pc);

	// モデルビュー変換行列の初期化
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMultMatrixd(Trackball.GetMatrix());

	// modelview matrixをぽーい
	glGetFloatv(GL_MODELVIEW_MATRIX, gl_m);
	glUniformMatrix4fv(glGetUniformLocation(multiProgram, "matM"), 1, GL_FALSE, gl_m);
	gl_normal[0] = gl_m[0]; gl_normal[1] = gl_m[1]; gl_normal[2] = gl_m[2];
	gl_normal[3] = gl_m[4]; gl_normal[4] = gl_m[5]; gl_normal[5] = gl_m[6];
	gl_normal[6] = gl_m[8]; gl_normal[7] = gl_m[9]; gl_normal[8] = gl_m[10];
	glUniformMatrix3fv(glGetUniformLocation(multiProgram, "matNormal"), 1, GL_FALSE, gl_normal);

	// 描画
	Objmesh.Draw();
	//glutSolidSphere(30, 100, 100);

	glFlush();

	//// 保存
	//glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
	//cv::Mat hh2(window_height, window_width, CV_8UC3);
	//glReadPixels(0, 0, hh2.cols, hh2.rows, GL_BGR, GL_UNSIGNED_BYTE, hh2.data); // RGBで取得
	//cv::flip(hh2, hh2, 0);

	// texture無効化
	glBindTexture(GL_TEXTURE_2D, 0);

	// シェーダを無効にする
	glUseProgram(0);

	// フレームバッファオブジェクトの結合を解除する
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// レンダーターゲットを元に戻す
	glDrawBuffer(GL_BACK);

	//**********************************************************
	// ofs render
	//**********************************************************
	// 描画先をフレームバッファオブジェクトに切り替える
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb_ofs);

	// レンダーターゲットを指定する
	glDrawBuffers(sizeof bufs_ofs / sizeof bufs_ofs[0], bufs_ofs);

	// viewport
	glViewport(0, 0, window_width, window_height);

	// 画面クリア
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// テクスチャマッピングを有効にする
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_RECTANGLE, cb_multi);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_RECTANGLE, ofsTextId);

	// シェーダプログラムの適用 
	glUseProgram(ofsProgram);
	glUniform1i(glGetUniformLocation(ofsProgram, "image"), 5);

	// モデルビュー行列とかの初期化
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// 図形の描画
	glNormal3d(0.0, 0.0, 1.0);
	glBegin(GL_QUADS);
	glTexCoord2d(0.0, 1.0); glVertex2d(-1.0, -1.0);
	glTexCoord2d(1.0, 1.0); glVertex2d(1.0, -1.0);
	glTexCoord2d(1.0, 0.0); glVertex2d(1.0, 1.0);
	glTexCoord2d(0.0, 0.0); glVertex2d(-1.0, 1.0);
	glEnd();

	glFlush();

	//// 保存
	//glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
	//cv::Mat hh2(window_height, window_width, CV_8UC3);
	//glReadPixels(0, 0, hh2.cols, hh2.rows, GL_BGR, GL_UNSIGNED_BYTE, hh2.data); // RGBで取得
	//cv::flip(hh2, hh2, 0);

	// texture無効化
	glBindTexture(GL_TEXTURE_2D, 0);

	// シェーダを無効にする
	glUseProgram(0);

	// フレームバッファオブジェクトの結合を解除する
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// レンダーターゲットを元に戻す
	glDrawBuffer(GL_BACK);

	//**********************************************************
	// main render
	//**********************************************************
	// viewport
	glViewport(0, 0, window_width, window_height);

	// 画面クリア
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// テクスチャマッピングを有効にする
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_RECTANGLE, cb_multi);
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_RECTANGLE, sb_multi);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_RECTANGLE, ab_multi);
	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_RECTANGLE, cb_ofs);

	// シェーダプログラムの適用 
	glUseProgram(glProgram);
	glUniform1i(glGetUniformLocation(glProgram, "image"), render_number);

	// モデルビュー行列とかの初期化
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// 図形の描画
	glNormal3d(0.0, 0.0, 1.0);
	glBegin(GL_QUADS);
	glTexCoord2d(0.0, 1.0); glVertex2d(-1.0, -1.0);
	glTexCoord2d(1.0, 1.0); glVertex2d(1.0, -1.0);
	glTexCoord2d(1.0, 0.0); glVertex2d(1.0, 1.0);
	glTexCoord2d(0.0, 0.0); glVertex2d(-1.0, 1.0);
	glEnd();

	// ダブルバッファリング
	glutSwapBuffers();

	// textureの無効化
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	// シェーダプログラムの使用終了
	glUseProgram(0);
}

// shader
void InitShader(){

	// glew初期化
	glewInit();

	//*****************************************************************
	// main shader
	//*****************************************************************
	// シェーダ読み込み
	MakeShader(glProgram, "test.vert", "test.frag");

	// 解除
	glUseProgram(0);

	//*****************************************************************
	// multiple shader
	//*****************************************************************
	MakeShader(multiProgram, "multi.vert", "multi.frag");

	glActiveTexture(GL_TEXTURE0);
	LoadGLTexture("data\\texture.png", multiTexId, false);

	// シェーダプログラムの適用 
	glUseProgram(multiProgram);
	glUniform1i(glGetUniformLocation(multiProgram, "tex1"), 0);

	glUseProgram(0);

	// カラーバッファ用のテクスチャを用意する
	glActiveTexture(GL_TEXTURE5);
	SetTexture(cb_multi, window_width, window_height, true);

	// 鏡面反射光を格納するテクスチャを用意する
	glActiveTexture(GL_TEXTURE6);
	SetTexture(sb_multi, window_width, window_height, true);

	// 環境光の反射光を格納するテクスチャを用意する
	glActiveTexture(GL_TEXTURE7);
	SetTexture(ab_multi, window_width, window_height, true);

	// デプスバッファ用のレンダーバッファを用意する
	glGenRenderbuffersEXT(1, &rb_multi);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, rb_multi);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, window_width, window_height);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

	// フレームバッファオブジェクトを作成する
	glGenFramebuffersEXT(1, &fb_multi);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb_multi);

	// フレームバッファオブジェクトにカラーバッファとしてテクスチャを結合する
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE, cb_multi, 0);

	// フレームバッファオブジェクトに拡散反射光の格納先のテクスチャを結合する
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_RECTANGLE, sb_multi, 0);

	// フレームバッファオブジェクトに環境光の反射光の格納先のテクスチャを結合する
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT2_EXT, GL_TEXTURE_RECTANGLE, ab_multi, 0);

	// フレームバッファオブジェクトにデプスバッファとしてレンダーバッファを結合する
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, rb_multi);

	// フレームバッファオブジェクトの結合を解除する
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	//*****************************************************************
	// ofscreen shader
	//*****************************************************************
	MakeShader(ofsProgram, "test.vert", "warp.frag");

	// ワーピング用のテクスチャ
	std::vector<GLfloat> map;
	for (int i = 0; i < window_width * window_height; ++i)
	{
		const GLfloat x = (GLfloat(i % window_width));
		const GLfloat y = (GLfloat(i / window_width));
		map.push_back(x + 30.0f * cos(20.0f * y / GLfloat(window_height)));
		map.push_back(y);
	}

	glActiveTexture(GL_TEXTURE2);
	glGenTextures(1, &ofsTextId);
	glBindTexture(GL_TEXTURE_RECTANGLE, ofsTextId);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RG32F, window_width, window_height, 0, GL_RG, GL_FLOAT, map.data());
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	// シェーダプログラムの適用 
	glUseProgram(ofsProgram);
	glUniform1i(glGetUniformLocation(ofsProgram, "warp"), 2);

	glUseProgram(0);

	// カラーバッファ用のテクスチャを用意する
	glActiveTexture(GL_TEXTURE10);
	SetTexture(cb_ofs, window_width, window_height, true);

	// デプスバッファ用のレンダーバッファを用意する
	glGenRenderbuffersEXT(1, &rb_ofs);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, rb_ofs);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, window_width, window_height);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

	// フレームバッファオブジェクトを作成する
	glGenFramebuffersEXT(1, &fb_ofs);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fb_ofs);

	// フレームバッファオブジェクトにカラーバッファとしてテクスチャを結合する
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE, cb_ofs, 0);

	// フレームバッファオブジェクトにデプスバッファとしてレンダーバッファを結合する
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, rb_ofs);

	// フレームバッファオブジェクトの結合を解除する
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

//********************************************************
// シェーダー初期化関連の関数
//********************************************************
// テクスチャの読み込み
void LoadGLTexture(string _fileName, GLuint &_texID, const bool _rectFlag){

	// 読み込み
	cv::Mat image = cv::imread(_fileName, -1);
	cv::flip(image, image, 0);
	if (image.channels() == 3){
		cv::cvtColor(image, image, CV_BGR2BGRA);
	}
	else if (image.channels() == 1){
		cv::cvtColor(image, image, CV_GRAY2BGRA);
	}

	// rect or?
	unsigned int texVersion;
	if (_rectFlag) texVersion = GL_TEXTURE_RECTANGLE;
	else texVersion = GL_TEXTURE_2D;

	if (_texID == -1){
		glGenTextures(1, &_texID);
		glBindTexture(texVersion, _texID);

		// テクスチャ画像はバイト単位に詰め込まれている 
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		//glTexParameteri(texVersion, GL_GENERATE_MIPMAP, GL_TRUE);

		// テクスチャの割り当て 
		glTexImage2D(texVersion, 0, GL_RGBA, image.cols, image.rows, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);

		// テクスチャを拡大・縮小する方法の指定 
		glTexParameteri(texVersion, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(texVersion, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		//glTexParameteri(texVersion, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		//glTexParameteri(texVersion, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		// テクスチャの繰り返し方法の指定 
		//glTexParameteri(texVersion, GL_TEXTURE_WRAP_S, GL_CLAMP);
		//glTexParameteri(texVersion, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(texVersion, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		glTexParameteri(texVersion, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		GLfloat an[3] = { 1.0, 1.0, 1.0 };
		glTexParameterfv(texVersion, GL_TEXTURE_BORDER_COLOR, an);

		// マテリアルの情報を使うかどうかの指定
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	}
	else{
		// bind
		glBindTexture(texVersion, _texID);

		// テクスチャの割り当て 
		glTexImage2D(texVersion, 0, GL_RGBA, image.cols, image.rows, 0, GL_BGRA, GL_UNSIGNED_BYTE, image.data);
	}

	// テクスチャのバインド解除
	glBindTexture(texVersion, 0);
}

// テクスチャのセット
void SetTexture(GLuint &_texID, const int _w, const int _h, const bool _rectFlag){

	// テクスチャの境界深度を指定する (最初の要素がデプステクスチャの外側の深度として使われる)
	const GLfloat borderDepth[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	// rect or?
	unsigned int texVersion;
	if (_rectFlag) texVersion = GL_TEXTURE_RECTANGLE;
	else texVersion = GL_TEXTURE_2D;

	glGenTextures(1, &_texID);
	glBindTexture(texVersion, _texID);
	glTexImage2D(texVersion, 0, GL_RGBA, _w, _h, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(texVersion, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(texVersion, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(texVersion, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(texVersion, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterfv(texVersion, GL_TEXTURE_BORDER_COLOR, borderDepth);
	glBindTexture(texVersion, 0);
}

// シェーダープログラムの初期化
void MakeShader(GLuint &_gl2Program, string _vertName, string _fragName)
{
	cout << "shader compile (" << _vertName << ", " << _fragName << ") -> ";

	// シェーダプログラムのコンパイル／リンク結果を得る変数 
	GLint compiled, linked;

	GLuint vertShader;
	GLuint fragShader;

	// シェーダオブジェクトの作成 
	vertShader = glCreateShader(GL_VERTEX_SHADER);
	fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	// シェーダのソースプログラムの読み込み 
	if (ReadShaderSource(vertShader, _vertName.c_str())) { getchar(); exit(1); }
	if (ReadShaderSource(fragShader, _fragName.c_str())) { getchar(); exit(1); }

	// バーテックスシェーダのソースプログラムのコンパイル 
	glCompileShader(vertShader);
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &compiled);
	PrintShaderInfoLog(vertShader);
	if (compiled == GL_FALSE) {
		fprintf(stderr, "Compile error in vertex shader.\n");
		getchar();
		exit(1);
	}

	// フラグメントシェーダのソースプログラムのコンパイル 
	glCompileShader(fragShader);
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &compiled);
	PrintShaderInfoLog(fragShader);
	if (compiled == GL_FALSE) {
		fprintf(stderr, "Compile error in fragment shader.\n");
		getchar();
		exit(1);
	}

	// プログラムオブジェクトの作成 
	_gl2Program = glCreateProgram();

	// シェーダオブジェクトのシェーダプログラムへの登録 
	glAttachShader(_gl2Program, vertShader);
	glAttachShader(_gl2Program, fragShader);

	// シェーダオブジェクトの削除 
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);

	// シェーダプログラムのリンク 
	glLinkProgram(_gl2Program);
	glGetProgramiv(_gl2Program, GL_LINK_STATUS, &linked);
	PrintProgramInfoLog(_gl2Program);
	if (linked == GL_FALSE) {
		fprintf(stderr, "Link error.\n");
		getchar();
		exit(1);
	}
	cout << "おしり" << endl;
}

// シェーダーのソースプログラムをメモリに読み込む
int ReadShaderSource(GLuint _shader, const char *_file){
	FILE *fp;
	const GLchar *source;
	GLsizei length;
	int ret;

	// ファイルを開く 
	fopen_s(&fp, _file, "rb");
	if (fp == NULL) {
		perror(_file);
		return -1;
	}

	// ファイルの末尾に移動し現在位置（つまりファイルサイズ）を得る 
	fseek(fp, 0L, SEEK_END);
	length = ftell(fp);

	// ファイルサイズのメモリを確保 
	source = (GLchar *)malloc(length);
	if (source == NULL) {
		fprintf(stderr, "Could not allocate read buffer.\n");
		return -1;
	}

	// ファイルを先頭から読み込む 
	fseek(fp, 0L, SEEK_SET);
	ret = fread((void *)source, 1, length, fp) != (size_t)length;
	fclose(fp);

	// シェーダのソースプログラムのシェーダオブジェクトへの読み込み 
	if (ret)
		fprintf(stderr, "Could not read file: %s.\n", _file);
	else
		glShaderSource(_shader, 1, &source, &length);

	// 確保したメモリの開放 
	free((void *)source);

	return ret;
}

// シェーダの情報を表示する
void PrintShaderInfoLog(GLuint _shader){
	GLsizei bufSize;

	glGetShaderiv(_shader, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1) {
		GLchar *infoLog;

		infoLog = (GLchar *)malloc(bufSize);
		if (infoLog != NULL) {
			GLsizei length;

			glGetShaderInfoLog(_shader, bufSize, &length, infoLog);
			fprintf(stderr, "InfoLog:\n%s\n\n", infoLog);
			free(infoLog);
		}
		else
			fprintf(stderr, "Could not allocate InfoLog buffer.\n");
	}
}

// プログラムの情報を表示する
void PrintProgramInfoLog(GLuint _program){
	GLsizei bufSize;

	glGetProgramiv(_program, GL_INFO_LOG_LENGTH, &bufSize);

	if (bufSize > 1) {
		GLchar *infoLog;

		infoLog = (GLchar *)malloc(bufSize);
		if (infoLog != NULL) {
			GLsizei length;

			glGetProgramInfoLog(_program, bufSize, &length, infoLog);
			fprintf(stderr, "InfoLog:\n%s\n\n", infoLog);
			free(infoLog);
		}
		else
			fprintf(stderr, "Could not allocate InfoLog buffer.\n");
	}
}


//********************************************************
// glutのコールバック関数
//********************************************************
void resize(int _w, int _h)
{
	// トラックボールする範囲
	Trackball.Resize(_w, _h);

	//// ウィンドウ全体をビューポートにする
	//glViewport(0, 0, _w, _h);

	//// 透視変換行列の指定
	//glMatrixMode(GL_PROJECTION);

	//// 透視変換行列の初期化 
	//glLoadIdentity();
	//gluPerspective(60.0, (double)_w / (double)_h, 1.0, 100.0);
}

void idle(void)
{
	// 画面の描き替え
	glutPostRedisplay();
}

void mouse(int _button, int _state, int _x, int _y)
{
	if (_button == GLUT_LEFT_BUTTON){
		if (_state == GLUT_DOWN){
			Trackball.Click(_x, _y);
		}
		else if (_state == GLUT_UP){
			Trackball.Up();
		}
		glutIdleFunc(idle);
		return;
	}
}

void motion(int _x, int _y)
{
	// トラックボール移動
	Trackball.Drag(_x, _y);
	glutPostRedisplay();
}

void keyboard(unsigned char _key, int _x, int _y)
{
	// ESC か q をタイプしたら終了
	if (_key == 'q' || _key == '\033'){
		cout << "おしり";
		exit(0);
	}
	else if (_key == 'R') Trackball.uni();
	else if (_key == 'c') capture_flag = true;
	else if (_key == '1') render_number = 5;
	else if (_key == '2') render_number = 6;
	else if (_key == '3') render_number = 7;
	else if (_key == '4') render_number = 10;

	glutPostRedisplay();
}








