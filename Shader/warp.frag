#version 330
// shadertype=<glsl>
// frag

// テクスチャ
uniform sampler2DRect image;
uniform sampler2DRect warp;

// フレームバッファに出力するデータ
layout (location = 0) out vec4 fc1; 

void main(void){
	vec4 tc = texture(warp, gl_FragCoord.xy + vec2(0.5f, 0.5f));
	fc1 = texture(image, tc.xy);
}